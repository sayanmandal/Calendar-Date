#include <iostream>
#include<exception>
#include<stdexcept>
#include "Date.h"
using namespace std;

int main()
{
    DateFormat format("dd-mmm-yyyy");
    cout<<"The Format is:dd-mmm-yyyy"<<endl;
    Date::setFormat(format);

    Date date;
    cout<<"Today is:"<<date<<endl;

    Date date_1((Day)10,(Month)12,2014);
    cout<<"The Date is:"<<date_1<<endl<<endl;

    DateFormat format_1("dd-mm-yy");
    cout<<"The Format is:dd-mm-yy"<<endl;
    Date::setFormat(format_1);

    cout<<"Today is:"<<date<<endl<<endl;
    cout<<"The Date is:"<<date_1<<endl<<endl;

    //Test For weekNumber
    Date date_n("30-12-2013");
    cout<<"The Date is:"<<date_n<<endl;
    cout<<(WeekNumber)date_n<<endl<<endl;

    Date date_N("1-1-2016");
    cout<<"The Date is:"<<date_N<<endl;
    cout<<(WeekNumber)date_N<<endl<<endl;
    //End for test for weekNumber

    DateFormat format_2("dd-mm-yyyy");
    cout<<"The Format is:dd-mm-yyyy"<<endl;
    Date::setFormat(format_2);

    cout<<"Today is:"<<date<<endl;

    DateFormat format_3("-mmm-yyyy");
    cout<<"The Format is:-mm-yyyy"<<endl;
    Date::setFormat(format_3);

    cout<<"Today is:"<<date<<endl<<endl;

    DateFormat format_4("dd--yyyy");
    cout<<"The Format is:dd--yyyy"<<endl;
    Date::setFormat(format_4);

    cout<<"Today is:"<<date<<endl;

    DateFormat format_5("dd-mmm-yy");
    cout<<"The Format is :dd-mmm-yy"<<endl;
    Date::setFormat(format_5);

    cout<<"Today is:"<<date<<endl<<endl;

    //Test For invalid argument
    try{
        Date date_4("29-02-2015");
    }catch(invalid_argument& e){
        cout<<e.what()<<endl;
    }catch(domain_error& e){
        cout<<e.what()<<endl;
    }catch(out_of_range& e){
        cout<<e.what()<<endl;
    }

    //Test for domain error
     try{
        Date date_4("29-02-2050");
    }catch(invalid_argument& e){
        cout<<e.what()<<endl;
    }catch(domain_error& e){
        cout<<e.what()<<endl;
    }catch(out_of_range& e){
        cout<<e.what()<<endl;
    }
    //Test For out of range
     try{
        Date date_4("29-03-2063");
    }catch(invalid_argument& e){
        cout<<e.what()<<endl;
    }catch(domain_error& e){
        cout<<e.what()<<endl;
    }catch(out_of_range& e){
        cout<<e.what()<<endl;
    }
    //Test For Parse Exception
    Date newdate("22-February-2016");
    cout<<endl<<"The Date is:"<<newdate<<endl;
    try{
        Date NewDate("22/02/2016");
        }catch(invalid_argument& e){
            cout<<e.what()<<endl;
        }catch(domain_error& e){
            cout<<e.what()<<endl;
        }catch(out_of_range& e){
            cout<<e.what()<<endl;
        }

    //Test For operator++
    Date testdate("26-May-2017");
    cout<<endl<<"The Date is:"<<testdate<<endl;
    cout<<"The Next Day is :"<<(++testdate)<<endl;

    Date again("29-02-2016");
    cout<<endl<<"The Date is:"<<again<<endl;
    cout<<"The Next Day is :"<<(++again)<<endl;

    Date so("31-Dec-2015");
    cout<<endl<<"The Date is:"<<so<<endl;
    cout<<"The Next Day is :"<<(++so)<<endl;

    //Test For the exception
    Date except("31-Dec-2049");
    cout<<endl<<"The Date is:"<<except<<endl;
    try{
        ++except;
    }catch(invalid_argument& e){
        cout<<e.what()<<endl;
    }catch(domain_error& e){
        cout<<e.what()<<endl;
    }catch(out_of_range& e){
        cout<<e.what()<<endl;
    }

    //Test For postfix ++
    Date todate("26-Feb-2016");
    cout<<"The Date is:"<<todate++<<endl;
    cout<<"The date after one week is:"<<todate<<endl;


    //Test for operator-- prefix
    Date testdate_1("26-May-2017");
    cout<<endl<<"The Date is:"<<testdate_1<<endl;
    cout<<"The previous Day is :"<<(--testdate_1)<<endl;

    Date again_1("29-02-2016");
    cout<<endl<<"The Date is:"<<again_1<<endl;
    cout<<"The previous Day is :"<<(--again_1)<<endl;

    Date so_1("31-Dec-2015");
    cout<<endl<<"The Date is:"<<so_1<<endl;
    cout<<"The previous Day is :"<<(--so_1)<<endl;

    //out of range exception
    Date except_1("1-Jan-1950");
    cout<<endl<<"The Date is:"<<except_1<<endl;
    try{
        --except_1;
    }catch(invalid_argument& e){
        cout<<e.what()<<endl;
    }catch(domain_error& e){
        cout<<e.what()<<endl;
    }catch(out_of_range& e){
        cout<<e.what()<<endl;
    }

    Date weekdate("3-1-2012");
    cout<<endl<<"The Date is:"<<weekdate--<<endl;
    cout<<"The Date before one week is :"<<weekdate<<endl<<endl;

    Date dateone("1-1-2015");
    cout<<endl<<"The First Date is: "<<dateone<<endl;

    Date datetwo("31-12-2016");
    cout<<"The Second Date is: "<<datetwo<<endl;

    cout<<"The Difference between two dates is :"<<(dateone-datetwo)<<endl;

    cout<<endl<<"See , The second date was :"<<datetwo<<endl;
    cout<<"So, After going back "<<(dateone-datetwo)<<" days,The date is :"<<(datetwo+(-(dateone-datetwo)))<<endl;
    cout<<"After forwarding "<<(dateone-datetwo)<<" days from "<<dateone<<" is :"<<(dateone+(dateone-datetwo))<<endl;

    cout<<datetwo<<" is on ";
    (WeekDay)datetwo;

    cout<<"The month of the "<<datetwo<<" is ";
    (Month)datetwo;

    cout<<(WeekNumber)datetwo<<endl;

    cout<<endl<<"Is "<<datetwo <<" in leapyear?"<<endl;
    cout<<std::boolalpha<<datetwo.leapYear()<<endl;

    cout<<endl<<"Is "<<datetwo <<" after "<<dateone<<" ?"<<endl;
    cout<<std::boolalpha<<(datetwo>dateone)<<endl;

    cout<<endl<<"Is "<<dateone <<" before "<<datetwo<<" ?"<<endl;
    cout<<std::boolalpha<<(dateone<datetwo)<<endl;

    Date dateinp;
    try{
    string inp;
    cout<<"Which format do you want?"<<endl;
    cin>>inp;
    DateFormat form(inp.c_str());
    Date::setFormat(form);
    cin>>dateinp;
    cout<<"The Date is :"<<dateinp<<endl;
    }catch(invalid_argument& e){
        cout<<e.what()<<endl;
    }catch(domain_error& e){
        cout<<e.what()<<endl;
    }catch(out_of_range& e){
        cout<<e.what()<<endl;
    }

    Date TestDate("30-12-2014");
    cout<<endl<<"The Date is: "<<TestDate<<endl;
    cout<<(WeekNumber)TestDate<<endl;


    Date TEST("1-1-1950");
    cout<<"The Date is: "<<TEST++<<endl;
    cout<<"One Week after the date is: "<<TEST++<<endl;
    //cout<<(WeekNumber)TEST<<endl;

    Date NewDate("31-12-2049");
    cout<<"The Date is :"<<NewDate<<endl;
    (WeekDay)NewDate;
    cout<<(WeekNumber)NewDate<<endl;

    cout<<endl<<"The DateFormat is :"<<Date::getFormat()<<endl;

    Date date_today("10-12-2014");
    cout<<"Today is :"<<date_today<<endl;
    cout<<(WeekNumber)date_today<<endl;

    return 0;
}
