#include<iostream>
#include<exception>
#include<cstring>
#include<stdexcept>
#include<string>
#include<sstream>
#include<ctime>
#include<cmath>
#include<algorithm>
#include "Date.h"

using namespace std;
Date copydate_n;
Date copyprev;

DateFormat Date::format;

DateFormat::DateFormat(const char* dateFormat, const char* monthFormat, const char* yearFormat){
    this->dateFormat = new char[strlen(dateFormat)+1];
    this->monthFormat = new char[strlen(monthFormat)+1];
    this->yearFormat = new char[strlen(yearFormat)+1];
    strcpy(this->dateFormat,dateFormat);
    strcpy(this->monthFormat,monthFormat);
    strcpy(this->yearFormat,yearFormat);
    }

DateFormat::DateFormat(const char* format){
    int cnt = 0,cntd=0,cntm=0,cnty=0;
    int first,second;
    int i;
    if(format!=NULL){
        for(i=0;i<strlen(format);i++){          //counting - and splitting the string with respect to '-'
            if(format[i]=='-'){
                if(cnt==0){
                    first = i;
                }
                if(cnt==1){
                    second = i;
                }
                cnt++;
            }
        }
        if(cnt<2 || cnt>2)
            throw invalid_argument("Invalid input:Exactly 2 '-' should be there ");
        if(cnt==2){
            for(int i = 0 ; i < first ; i++){
                if(format[i]=='d'){
                    cntd++;                 //counting 'd'
                }
            }
            if(cntd!=0 && cntd<=2){
                this->dateFormat=new char[cntd+1];
                for(int i = 0 ; i < cntd ; i++){
                    dateFormat[i]='d';
                }
                dateFormat[cntd]='\0';
            }if(cntd==0){
                this->dateFormat=new char[2];
                strcpy(this->dateFormat," ");
                }if(cntd>2){
                    throw invalid_argument("Invalid Input");
                    }
            for(int i = first+1 ; i < second ; i++){
                if(format[i]=='m'){
                    cntm++;              //counting 'm'
                }
            }
            if(cntm!=0&&cntm<=3){
                this->monthFormat=new char[cntm+1];
                for(int i = 0 ; i < cntm ; i++){
                    monthFormat[i]='m';
                }
                monthFormat[cntm]='\0';
            }if(cntm==0){
                this->monthFormat=new char[2];
                strcpy(this->monthFormat," ");
            }if(cntm>3){
                    throw invalid_argument("Invalid Input");
                    }
            for(int i = second+1 ; i < strlen(format) ; i++){
                if(format[i]=='y'){
                    cnty++;                  //counting 'y'
                }
            }
            if(cnty!=0&&(cnty==2||cnty==4)){
                this->yearFormat=new char[cnty+1];
                for(int i = 0 ; i < cnty ; i++){
                    yearFormat[i]='y';
                }
                yearFormat[cnty]='\0';
            }if(cnty==0){
                this->yearFormat=new char[2];
                strcpy(this->yearFormat," ");
            }if(cnty>4||cnty==3){
                    throw invalid_argument("Invalid Input"); //If number of '-' is >=3
                    }
        }
    }
    }

//copy constructor

DateFormat::DateFormat(const DateFormat& form){
     DateFormat& forma = const_cast<DateFormat&>(form);
     dateFormat = new char[forma.getdateFormat().length()+1];
     monthFormat = new char[forma.getmonthFormat().length()+1];
     yearFormat = new char[forma.getyearFormat().length()+1];

     string s = forma.getdateFormat();
     for(int i = 0 ; i < forma.getdateFormat().length() ; i++){ //copying char by char
        dateFormat[i] = s[i];
     }
     dateFormat[forma.getdateFormat().length()] = '\0';
     string t = forma.getmonthFormat();
     for(int i = 0 ; i < forma.getmonthFormat().length() ; i++){
     monthFormat[i] = t[i];
     }
     monthFormat[forma.getmonthFormat().length()]='\0';
     string r = forma.getyearFormat();
     for(int i = 0 ; i < forma.getyearFormat().length() ; i++){
        yearFormat[i] = r[i];
     }
     yearFormat[forma.getyearFormat().length()]='\0';
    }


//copy assignment operator
DateFormat& DateFormat::operator=(const DateFormat& form){
    DateFormat& forma = const_cast<DateFormat&>(form);
    if(this!=&form){           //checking self assignment
        delete[] dateFormat;
        delete[] monthFormat;
        delete[] yearFormat; //next same as copy constructor
        dateFormat = new char[forma.getdateFormat().length()+1];
        monthFormat = new char[forma.getmonthFormat().length()+1];
        yearFormat = new char[forma.getyearFormat().length()+1];
        string s = forma.getdateFormat();
        for(int i = 0 ; i < forma.getdateFormat().length() ; i++){
            dateFormat[i] = s[i];
        }
        dateFormat[forma.getdateFormat().length()] = '\0';
        string t = forma.getmonthFormat();
        for(int i = 0 ; i < forma.getmonthFormat().length() ; i++){
            monthFormat[i] = t[i];
        }
        monthFormat[forma.getmonthFormat().length()]='\0';
        string r = forma.getyearFormat();
        for(int i = 0 ; i < forma.getyearFormat().length() ; i++){
            yearFormat[i] = r[i];
        }
        yearFormat[forma.getyearFormat().length()]='\0';
    }
    }
//default constructor
DateFormat::DateFormat(){
    delete[] dateFormat;
    delete[] monthFormat;
    delete[] yearFormat;
    dateFormat = new char[3];
    strcpy(dateFormat,"dd");
    monthFormat = new char[4];
    strcpy(monthFormat,"mmm");
    yearFormat = new char[3];
    strcpy(yearFormat,"yy");
    }

DateFormat::~DateFormat(){
    delete[] dateFormat;     //To prevent memory leakage
    delete[] monthFormat;
    delete[] yearFormat;
    }

//Here I am creating the getters for the private members of DateFormat class

string DateFormat::getdateFormat(){
    string thestring(dateFormat);
    return thestring;
    }

string DateFormat::getmonthFormat(){
    string thestring(monthFormat);
    return thestring;
    }

string DateFormat::getyearFormat(){
    string thestring(yearFormat);
    return thestring;
    }

Date::Date(Day d, Month m, Year y)throw(invalid_argument,domain_error, out_of_range){
    if(y>=1950&&y<=2049){ //It will consider 29-Feb-2015 ,31-Apr-2016 this kind of thing,invalid input
    if(d>D31||m>Dec||(m==Jan&&d>D31)||(y%4==0 && m==Feb && d>D29)||(y%4!=0 && m==Feb && d>28)||(m==Mar&&d>D31)||(m==Apr&&d>D30)||(m==May&&d>D31)||(m==Jun&&d>D30)||(m==Jul&&d>D31)||(m==Aug&&d>D31)||(m==Sep&&d>D30)||(m==Oct&&d>D31)||(m==Nov&&d>D30)||(m==Dec&&d>D31)){
        throw invalid_argument("Invalid Argument");
    }
    }
    if(y<1950||y>2049){  //Domain Error will be thrown when it is error as a triplet eg. (29,Feb,2053)
        if((m==Jan&&d>D31)||(m==Mar&&d>D31)||(m==Apr&&d>D30)||(m==May&&d>D31)||(m==Jun&&d>D30)||(m==Jul&&d>D31)||(m==Aug&&d>D31)||(m==Sep&&d>D30)||(m==Oct&&d>D31)||(m==Nov&&d>D30)||(m==Dec&&d>D31)){
            throw domain_error("Domain Error");
        }
        if(y%4==0){
            if(m==Feb&&d>29)
                throw domain_error("Domain Error");
            }else{
                if(m==Feb&&d>28)
                    throw domain_error("Domain Error");
            }
    }
    if(y<1950||y>2049){ //If the year is less than 1950 or it is greater than 2049
        throw out_of_range("Out Of Range");
    }
    this->d = d;
    this->m = m;
    this->y = y;
}

Date::Date(const char* date)throw(invalid_argument,domain_error, out_of_range){
    string the_date(date);
    stringstream sso;
    stringstream sso1;
    stringstream sso2;    //for converting string to int

    if(the_date.find('-') == std::string::npos)  //If no - is there
        throw invalid_argument("Invalid Input:Exactly 2 '-' should be there");

    size_t n = std::count(the_date.begin(),the_date.end(),'-'); //It will only consider dd-mm-yyyy this kind of thing
    if(n!=2)
        throw invalid_argument("Invalid Input:Exactly 2 '-' should be there");

    string token = the_date.substr(0,the_date.find("-"));//it will first find the substring where it encounters the first '-'

    //converting from string to int
    int the_day;
    sso<<token;
    sso>>the_day;
    Day da = (Day)the_day;
    the_date.erase(0,the_date.find("-")+1);//It will erase that substring

    Month mo;
    string token1 = the_date.substr(0,the_date.find("-"));//then again same thing
    //for month input as "Jan" or "January"
    if(token1.compare("Jan")==0||token1.compare("January")==0)
        mo = Jan;
    else if(token1.compare("Feb")==0||token1.compare("February")==0)
        mo = Feb;
    else if(token1.compare("Mar")==0||token1.compare("March")==0)
        mo = Mar;
    else if(token1.compare("Apr")==0||token1.compare("April")==0)
        mo = Apr;
    else if(token1.compare("May")==0||token1.compare("May")==0)
        mo = May;
    else if(token1.compare("Jun")==0||token1.compare("June")==0)
        mo = Jun;
    else if(token1.compare("Jul")==0||token1.compare("July")==0)
        mo = Jul;
    else if(token1.compare("Aug")==0||token1.compare("August")==0)
        mo = Aug;
    else if(token1.compare("Sept")==0||token1.compare("September")==0)
        mo = Sep;
    else if(token1.compare("Oct")==0||token1.compare("October")==0)
        mo = Oct;
    else if(token1.compare("Nov")==0||token1.compare("November")==0)
        mo = Nov;
    else if(token1.compare("Dec")==0||token1.compare("December")==0)
        mo = Dec;
    else{
    int the_month;
    sso1<<token1;
    sso1>>the_month;
    mo = (Month)the_month;
    }
    if(mo == 0)//for other invalid inputs
        throw invalid_argument("Invalid Input");

    the_date.erase(0,the_date.find("-")+1);//finding the year
    int the_year;
    sso2<<the_date;
    sso2>>the_year;
    Year yr = (Year)the_year;
    if(yr>=1950&&yr<=2049){
    if(da>D31||mo>Dec||(mo==Jan&&da>D31)||(yr%4==0&&mo==Feb&&da>D29)||(yr%4!=0&&mo==Feb&&da>D28)||(mo==Mar&&da>D31)||(mo==Apr&&da>D30)||(mo==May&&da>D31)||(mo==Jun&&da>D30)||(mo==Jul&&da>D31)||(mo==Aug&&da>D31)||(mo==Sep&&da>D30)||(mo==Oct&&da>D31)||(mo==Nov&&da>D30)||(mo==Dec&&da>D31))
        throw invalid_argument("Invalid Argument");
    }

    if(yr<1950||yr>2049){
        if((mo==Jan&&da>D31)||(mo==Mar&&da>D31)||(mo==Apr&&da>D30)||(mo==May&&da>D31)||(mo==Jun&&da>D30)||(mo==Jul&&da>D31)||(mo==Aug&&da>D31)||(mo==Sep&&da>D30)||(mo==Oct&&da>D31)||(mo==Nov&&da>D30)||(mo==Dec&&da>D31)){
            throw domain_error("Domain Error");
        }
        if(yr%4==0){
            if(mo==Feb&&da>29)
                throw domain_error("Domain Error");
            }else{
                if(mo==Feb&&da>28)
                    throw domain_error("Domain Error");
            }
    }

    if(yr<1950||yr>2049)
        throw out_of_range("Out Of Range");

    this->d = da;
    this->m = mo;
    this->y = yr;
    }
//Default date constructor which will print today's date
Date::Date() throw(domain_error,out_of_range){
    time_t now = time(0);
    tm *ltm = localtime(&now);
    Year yr = (Year)(1900+ltm->tm_year);
    Day da = (Day)ltm->tm_mday;
    Month mo = (Month)(ltm->tm_mon+1);


    if(yr<1950||yr>2049){
        if((mo==Jan&&da>D31)||(mo==Mar&&da>D31)||(mo==Apr&&da>D30)||(mo==May&&da>D31)||(mo==Jun&&da>D30)||(mo==Jul&&da>D31)||(mo==Aug&&da>D31)||(mo==Sep&&da>D30)||(mo==Oct&&da>D31)||(mo==Nov&&da>D30)||(mo==Dec&&da>D31)){
            throw domain_error("Domain Error");
        }
        if(yr%4==0){
            if(mo==Feb&&da>29)
                throw domain_error("Domain Error");
            }else{
                if(mo==Feb&&da>28)
                    throw domain_error("Domain Error");
            }
    }

    if(yr<1950||yr>2049)
        throw out_of_range("Out Of Range");

    this->m = mo;
    this->d = da;
    this->y = yr;

    }
//copy constructor
Date::Date(const Date& date){
    d = (const_cast<Date&>(date)).getDay();
    m = (const_cast<Date&>(date)).getMonth();
    y = (const_cast<Date&>(date)).getYear();
    }

Date::~Date(){
    }
//copy assignment operator
Date& Date::operator=(const Date& date){
    d = (const_cast<Date&>(date)).getDay();
    m = (const_cast<Date&>(date)).getMonth();
    y = (const_cast<Date&>(date)).getYear();
    return *this;
    }
//prefix operator
Date& Date::operator++()throw (out_of_range){
    if(m==Jan && d==D31){
        d = D01;
        m = Feb;
    }
    else if(m==Feb&&d==D29&&y%4==0){
        d = D01;
        m = Mar;
    }else if(m==Feb && d==D28 &&y%4!=0){
        d = D01;
        m = Mar;
    }else if(m==Mar && d==D31 ){
        d = D01;
        m = Apr;
    }else if(m==Apr && d==D30){
        d = D01;
        m = May;
    }else if(m==May && d==D31){
        d = D01;
        m = Jun;
    }else if(m==Jun && d==D30){
        d = D01;
        m = Jul;
    }else if(m==Jul && d==D31){
        d = D01;
        m = Aug;
    }else if(m==Aug && d==D31){
        d = D01;
        m = Sep;
    }else if(m==Sep && d==D30){
        d = D01;
        m = Oct;
    }else if(m==Oct && d==D31){
        d = D01;
        m = Nov;
    }else if(m==Nov && d==D30){
        d = D01;
        m = Dec;
    }else if(m==Dec && d==D31){
        d = D01;
        m = Jan;
        y = y+1;
        if(y>2049)//if the date is 31-12-2049 then ++ operator will give out_of_range
            throw out_of_range("Out Of Range");
    }else{
        d = (Day)(d+1);
        }
    return *this;
    }
//prefix operator
Date& Date::operator--()throw(out_of_range){
    if(m==Jan && d==D01){
        d = D31;
        m = Dec;
        y--;
        if(y<1950)//If the date is 1-1-1950 then -- operator will give out_of_range
            throw out_of_range("Out Of Range");
    }
    else if(m==Feb&&d==D01){
        d = D31;
        m = Jan;
    }else if(m==Mar && d==D01){
        if(y%4==0){
        d = D29;
        m = Feb;
        }else{
            d = D28;
            m = Feb;
            }
    }else if(m==Apr && d==D01){
        d = D31;
        m = Mar;
    }else if(m==May && d==D01){
        d = D30;
        m = Apr;
    }else if(m==Jun && d==D01){
        d = D31;
        m = May;
    }else if(m==Jul && d==D01){
        d = D30;
        m = Jun;
    }else if(m==Aug && d==D01){
        d = D31;
        m = Jul;
    }else if(m==Sep && d==D01){
        d = D31;
        m = Aug;
    }else if(m==Oct && d==D01){
        d = D30;
        m = Sep;
    }else if(m==Nov && d==D01){
        d = D31;
        m = Oct;
    }else if(m==Dec && d==D01){
        d = D30;
        m = Nov;
    }else{
        d = (Day)(d-1);
        }
    return *this;
    }

//postfix operator
Date& Date::operator++(int j)throw(out_of_range){
    int i;
    copydate_n = *this;
    for(i=0;i<7;i++){  //I am just calling prefix operator++ 7 times
        ++(*this);
    }
    return copydate_n;
    }

Date& Date::operator--(int j)throw(out_of_range){
    int i;
    copyprev = *this;
    for(i=0;i<7;i++){  //I am just calling prefix operator-- 7 times
        --(*this);
    }
    return copyprev;
}

bool Date::operator==(const Date& otherDate){
    if(this->d == otherDate.d && this->m == otherDate.m && this->y == otherDate.y)
        return true;
    return false;
    }

bool Date::operator!=(const Date& otherDate){
    if(this->d != otherDate.d || this->m != otherDate.m || this->y != otherDate.y)
        return true;
    return false;
    }

bool Date::operator<(const Date& otherDate){
    if(this->y < otherDate.y)
        return true;
    else if(this->y == otherDate.y){
        if(this->m <otherDate.m)
            return true;
        else if(this->m == otherDate.y){
            if(this->d < otherDate.d)
                return true;
        }
    }
    return false;
    }

bool Date::operator<=(const Date& otherDate){
    if((*this) < otherDate || (*this) == otherDate)
        return true;
    return false;
    }

bool Date::operator>(const Date& otherDate){
    if(this->y > otherDate.y)
        return true;
    else if(this->y == otherDate.y){
        if(this->m >otherDate.m)
            return true;
        else if(this->m == otherDate.y){
            if(this->d > otherDate.d)
                return true;
        }
    }
    return false;
}

bool Date::operator>=(const Date& otherDate){
    if((*this)==otherDate||(*this)>otherDate)
        return true;
    return false;
    }

//difference between two dates
unsigned int Date::operator-(const Date& otherDate){
    const int monthDays[12] = {31, 28, 31, 30, 31, 30,
                           31, 31, 30, 31, 30, 31};
    Date& other = const_cast<Date&>(otherDate);
    //Here I am counting the number of days of the date since 00/00/0000, so every leap year contributes an extra day.
    //so main thing is to count the number of leap years from 00/00/0000 to today's date
    long int n1 = (other.getYear()*365)+ other.getDay();

    for(int i = 0 ; i < other.getMonth()-1 ; i++)
        n1 += monthDays[i];

    unsigned int years = other.getYear();

    if(other.getMonth()<=Feb)
        years--; //If the month is greater than Feb then an extra day will be added

    n1 += (years/4 - years/100 + years/400);//number of leap years

    //same algorithm with the another date
    long int n2 = (this->getYear()*365)+ this->getDay();

    for(int i = 0 ; i < this->getMonth()-1 ; i++)
        n2 += monthDays[i];

    unsigned int years1 = this->getYear();

    if(this->getMonth()<=Feb)
        years1--;

    n2 += (years1/4 - years1/100 + years1/400);

    if(n1>n2) //return the absolute value
        return (n1-n2);
    else
        return (n2-n1);
    }
//operator+ using operator++
Date Date::operator+(int noOfDays)throw(domain_error, out_of_range){
    int i;
    Date date = *this;
    if(noOfDays<0){
        for(i=0;i<(-noOfDays);i++)
            --date;
    }else{
        for(i=0;i<noOfDays;i++)
            ++date;
    }
    if(date.y<1950||date.y>2049){
        if((date.m==Jan&&date.d>D31)||(date.m==Mar&&date.d>D31)||(date.m==Apr&&date.d>D30)||(date.m==May&&date.d>D31)||(date.m==Jun&&date.d>D30)||(date.m==Jul&&date.d>D31)||(date.m==Aug&&date.d>D31)||(date.m==Sep&&date.d>D30)||(date.m==Oct&&date.d>D31)||(date.m==Nov&&date.d>D30)||(date.m==Dec&&date.d>D31)){
            throw domain_error("Domain Error");
        }
        if(date.y%4==0){
            if(date.m==Feb&&date.d>29)
                throw domain_error("Domain Error");
            }else{
                if(date.m==Feb&&date.d>28)
                    throw domain_error("Domain Error");
            }
    }
    if(date.y<1950||date.y>2049){
        throw out_of_range("Out Of Range");
    }
    return date;
    }

Date::operator WeekDay() const{
    Date date = *this;
    date.setDay(D01);
    date.setMonth(Jan);
    date.setYear(this->y);
    unsigned int wn = (4 + ((y-1925+(y-1925)/4)%7)); //I am calculating what day will be the 1st of January since Thursday
    WeekDay wd = (WeekDay)(wn%7!=0?(wn%7):7);
    Date newdate = *this;
    unsigned int t = (((wd+((newdate-date) %7))%7)!=0?((wd+((newdate-date) %7))%7):7);
    switch(t){
    case 1:
        cout << "Monday"<<endl;
        break;
    case 2:
        cout << "Tuesday"<< endl;
        break;
    case 3:
        cout << "Wednesday"<< endl;
        break;
    case 4:
        cout << "Thursday" << endl;
        break;
    case 5:
        cout << "Friday" << endl;
        break;
    case 6:
        cout << "Saturday" << endl;
        break;
    case 7:
        cout << "Sunday" << endl;
        break;
        }
    return  (WeekDay)(((wd+((newdate-date) %7))%7)!=0?((wd+((newdate-date) %7))%7):7);
    }

Date::operator WeekNumber() const{
    Date date = *this; //copy of this date
    Date copy_date = *this;
    date.setDay(D01);
    date.setMonth(Jan);
    date.setYear(this->y);
    unsigned int wn = (4 + ((y-1925+(y-1925)/4)%7)); //this
    try{
    Date copy_date_1 = copy_date+3;
    Date copy_date_2 = copy_date+2;
    Date copy_date_3 = copy_date+1;
    WeekDay WD = (WeekDay)copy_date_1;
    WeekDay WD_1 = (WeekDay)copy_date_2;
    WeekDay WD_2 = (WeekDay)copy_date_3;
    Month MO = (Month)copy_date_1;
    Year YR = copy_date_1.getYear();
    Month MO_1 = (Month)copy_date_2;
    Year YR_1 = copy_date_2.getYear();
    Month MO_2 = (Month)copy_date_3;
    Year YR_2 = copy_date_3.getYear();
    if((WD==Thr && MO==Jan && YR == y+1)||(WD_1==Thr && MO_1==Jan && YR_1 == y+1)||(WD_2==Thr && MO_2==Jan && YR_2 == y+1))
        return (WeekNumber)1;
    while((wn%7)!= 4){
        ++date;
        wn++;
    }

    try{
    Date date2 = date+(-3);
    Date date1 = *this;
    if(date2<*this){
        return (WeekNumber)((((date1 - date2))/7)+1);
        }
    else{
        Date date_1 = *this;
        date_1.setDay(D01);
        date_1.setMonth(Jan);
        date_1.setYear(this->y - 1);
        unsigned int wn1 =(4 + ((y-1-1925+(y-1-1925)/4)%7));
        while((wn1%7)!= 4){
            ++date_1;
            wn1++;
        }
        Date date_2 = date_1+(-3);
        Date date_ag = *this;
        return (WeekNumber)((((date_ag - date_2))/7)+1);
    }}catch(out_of_range& e){
        return (WeekNumber)53;
    }}catch(out_of_range& e){
        return (WeekNumber)52;
    }
    }

Date::operator Month() const{
switch(m){
case Jan:
    cout<<"January"<<endl;
    break;
case Feb:
    cout<<"February"<<endl;
    break;
case Mar:
    cout<<"March"<<endl;
    break;
case Apr:
    cout<<"April"<<endl;
    break;
case May:
    cout<<"May"<<endl;
    break;
case Jun:
    cout<<"June"<<endl;
    break;
case Jul:
    cout<<"July"<<endl;
    break;
case Aug:
    cout<<"August"<<endl;
    break;
case Sep:
    cout<<"September"<<endl;
    break;
case Oct:
    cout<<"October"<<endl;
    break;
case Nov:
    cout<<"November"<<endl;
    break;
case Dec:
    cout<<"December"<<endl;
    break;
}
    return m;
    }


bool Date::leapYear() const{
    if(y%4==0)
        return true;
    return false;
    }

void Date::setFormat(DateFormat& form){
    DateFormat format_1(form);
    format = format_1;
    }

DateFormat& Date::getFormat(){
    return format;
    }

void Date::setDay(Day d){
    this->d = d;
    }
void Date::setMonth(Month m){
    this->m = m;
    }
void Date::setYear(Year y){
    this->y = y;
    }

Day Date::getDay(){
    return d;
    }
Month Date::getMonth(){
    return m;
    }
Year Date::getYear(){
    return y;
    }

ostream& operator<<(ostream& out , const Date& date1){
    Date& date = const_cast<Date&>(date1);
    string dateformat = date.getFormat().getdateFormat();
    string monthformat = date.getFormat().getmonthFormat();
    string yearformat = date.getFormat().getyearFormat();
    int countday = 0;
    for(int i = 0 ; i < dateformat.length() ; i++){
        if(dateformat[i] == 'd')
            countday++;
    }
    int countmonth = 0;
    for(int i = 0 ; i < monthformat.length() ; i++){
        if(monthformat[i]=='m')
            countmonth++;
    }
    int countyear = 0;
    for(int i = 0 ; i < yearformat.length() ; i++){
        if(yearformat[i]=='y')
            countyear++;
    }

    switch(countday){
    case 0:
        out<<"";
        break;
    case 1:
        out<<date.getDay();
        break;
    case 2:
        if(date.getDay()<10){
            out<<0<<date.getDay();
            break;
        }else{
            out<<date.getDay();
            break;
        }

    }
    out<<"-";
    if(countmonth==0){
    switch(date.getMonth()){
    case Jan:
        out<<"January";
        break;
    case Feb:
        out<<"February";
        break;
    case Mar:
        out<<"March";
        break;
    case Apr:
        out<<"April";
        break;
    case May:
        out<<"May";
        break;
    case Jun:
        out<<"June";
        break;
    case Jul:
        out<<"July";
        break;
    case Aug:
        out<<"August";
        break;
    case Sep:
        out<<"September";
        break;
    case Oct:
        out<<"October";
        break;
    case Nov:
        out<<"November";
        break;
    case Dec:
        out<<"December";
        break;
        }
    }else if(countmonth==1){
        out<<date.getMonth();
        }else if(countmonth==2){
            if(date.getMonth()<10)
                cout<<0<<date.getMonth();
            else
                cout<<date.getMonth();
            }else{
    switch(date.getMonth()){
    case Jan:
        out<<"Jan";
        break;
    case Feb:
        out<<"Feb";
        break;
    case Mar:
        out<<"Mar";
        break;
    case Apr:
        out<<"Apr";
        break;
    case May:
        out<<"May";
        break;
    case Jun:
        out<<"Jun";
        break;
    case Jul:
        out<<"Jul";
        break;
    case Aug:
        out<<"Aug";
        break;
    case Sep:
        out<<"Sept";
        break;
    case Oct:
        out<<"Oct";
        break;
    case Nov:
        out<<"Nov";
        break;
    case Dec:
        out<<"Dec";
        break;
        }
            }
    out<<"-";
    if(countyear==0){
        out<<"";
    }else if(countyear==2){
        out<<(date.getYear()%100);
        }else{
        out<<(date.getYear());
        }
    return out;
    }

istream& operator>>(istream& in , Date& date){
    cout<<"Press 1 for Entering day,month and year(Please follow the order strictly)"<< endl;
    cout<<"Press 2 for Entering date in string format"<<endl;
    int inp;
    cin>>inp;
    if(inp==1){
    cout<<"Enter day,month and year(Please follow the order strictly)"<<endl;
    int day;
    int month;
    cout << "Enter the day"<<endl;
    in>>day;
    cout << "Enter the Number of the month"<<endl;
    in>>month;
    cout << "Enter the Year"<<endl;
    in>>date.y;
    date.setDay((Day)day);
    date.setMonth((Month)month);
    }else if (inp==2){
        string d;
        cout<<"Enter the Date"<< endl;
        cin.ignore();
        getline(cin,d);
        Date date2(d.c_str());
        date=date2;
        }else{
            cout << "You did not enter 1 or 2"<< endl;
            }
      return in;
    }

ostream& operator<<(ostream& out , const DateFormat& dateform){
    DateFormat& datefor = const_cast<DateFormat&>(dateform);
    out<<datefor.getdateFormat()<<"-"<<datefor.getmonthFormat()<<"-"<<datefor.getyearFormat();
    return out;
    }
